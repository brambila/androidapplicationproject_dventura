package com.twopointb.transportproj;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Random rand = new Random();
        int cityLength = 10;
        int cityWidth = 10;
        City myCity = new City(cityLength, cityWidth);
        Car car = myCity.addCarToCity(rand.nextInt(cityLength - 1),rand.nextInt(cityWidth - 1));
        Passenger passenger = myCity.addPassengerToCity(rand.nextInt(cityLength - 1),rand.nextInt(cityWidth - 1),rand.nextInt(cityLength - 1),rand.nextInt(cityWidth - 1));
        while (!passenger.isAtDestination()) {
            tick(car,passenger);
        }
        System.out.println("Done");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Takes one action (move the car one spot or pick up the passenger).
     *
     *  @param car The car to move
     *  @param passenger The passenger to pick up
     */
    private void tick(Car car, Passenger passenger) {

    }


}
